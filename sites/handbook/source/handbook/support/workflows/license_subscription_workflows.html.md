---
layout: handbook-page-toc
title: Licensing & Subscription Workflows
category: License and subscription
---

The license and subscription workflows handled by L&R-focused Support Engineers
have been moved to a [dedicated L&R workflows page](../license-and-renewals/workflows).
